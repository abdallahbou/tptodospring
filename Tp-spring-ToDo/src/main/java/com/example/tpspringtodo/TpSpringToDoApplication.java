package com.example.tpspringtodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSpringToDoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpSpringToDoApplication.class, args);
	}

}
