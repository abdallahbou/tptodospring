package com.example.tpspringtodo.controller;

import com.example.tpspringtodo.entity.Todo;
import com.example.tpspringtodo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/todo")
@ResponseBody
public class TodoController {

    @Autowired
    TodoService todoService;

    @GetMapping("")
    public List<Todo> getAllTodo(){
        return todoService.findAll();
    }

    @GetMapping("/{id}")
    public Todo getTodo(@PathVariable("id") Integer id){

        return  todoService.findById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteTodo(@PathVariable("id") Integer id){
        Todo t = todoService.findById(id);
        if(t !=null && todoService.delete(t)){
            return "Suppression ok";
        }
        return "Aucun produit avec cet id";
    }
    @PostMapping("")
    public Todo postTodo(@RequestBody  Todo todo){
        if(todoService.create(todo)){
            return todo;
        }
        return null;
    }


    @PostMapping("/{id}")
    public  Todo updateTodo(@PathVariable("id") Integer id,@RequestBody Todo todo){
        Todo updateTodo = todoService.findById(id);
        if(updateTodo != null){
            updateTodo.setDate(todo.getDate());
            updateTodo.setEtat(todo.getEtat());
            updateTodo.setDescription(todo.getDescription());
            updateTodo.setTitre(todo.getTitre());
            if(todoService.update(updateTodo)){
                return updateTodo;
            }
        }

        return updateTodo;

    }


    


}
