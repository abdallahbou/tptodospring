package com.example.tpspringtodo.service;

import com.example.tpspringtodo.entity.Todo;
import com.example.tpspringtodo.interfaces.IDAO;
import com.example.tpspringtodo.tools.ServiceHibernate;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class TodoService implements IDAO<Todo> {

    private ServiceHibernate serviceHibernate;
    private Session session;

    public TodoService(ServiceHibernate serviceHibernate){
        this.serviceHibernate = serviceHibernate;
        session= this.serviceHibernate.getSession();
    }

    @Override
    public boolean create(Todo o) {

        session.beginTransaction();
        session.persist(o);
        session.getTransaction().commit();

        return true;
    }

    @Override
    public boolean delete(Todo o) {

        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Todo o) {

        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Todo findById(int id) {
        Todo todo= null;

        todo = (Todo) session.get(Todo.class,id);

        return todo;
    }

    @Override
    public List<Todo> findAll() {

        Query<Todo> todoQuery = session.createQuery("from Todo", Todo.class);

        return todoQuery.list();
    }
}
